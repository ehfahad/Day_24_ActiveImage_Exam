<?php
namespace App\Bitm\SEIP1020\Utility;

class Utility{

    public static function d($data=''){

        echo "<pre>";
        echo var_dump($data);
        echo "</pre>";

    }

    public static function dd($data=''){

        echo "<pre>";
        echo var_dump($data);
        echo "</pre>";
        die();

    }

    public static function redirect($data){
        header('Location:'.$data);
    }



}