<?php

namespace App\Bitm\SEIP1020\Hobby;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;
class Hobby{
    public $id="";
    public $name="";
    public $hobby="";
    public $conn;
    public $deleted_at="";

public function prepare($data=""){
    if(array_key_exists('name',$data)){
        $this->name=filter_var($data['name'], FILTER_SANITIZE_STRING);
    }
    if(array_key_exists('Hobby',$data)){
        $this->hobby=filter_var($data['Hobby'], FILTER_SANITIZE_STRING);
    }

    if (array_key_exists('id',$data)){
        $this->id=$data['id'];
    }


    return $this;
}


    public function __construct()
    {
        $this->conn= mysqli_connect('localhost','root','','atomicprojectp5') or die("Cannot connect");
    }

    public function store(){
        $query="INSERT INTO `hobby` (`id`, `name`, `hobby`) VALUES (NULL, '".$this->name."','".$this->hobby."')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        //echo $result;
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully
</div>");
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }

    }



    public function index(){
        $allResult=array();
        $query= "SELECT * FROM `hobby`";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $allResult[]=$row;
        }
        return $allResult;
    }

    public function show(){

        $query= "SELECT * FROM `hobby` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `hobby` SET `name` = '".$this->name."', `hobby` = '".$this->hobby."' WHERE `hobby`.`id` =".$this->id;
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        //echo $result;
        if($result){
            Message::message('<div class="alert alert-info">
                                    <strong>Info!</strong> Data has been updated successfully
                              </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }

    }

    public function delete(){
        $query="DELETE FROM `hobby` WHERE `hobby`.`id` =".$this->id;
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        //echo $result;
        if($result){
            Message::message('<div class="alert alert-danger">
                                    <strong>Info!</strong> Data has been deleted successfully
                              </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }

    }


    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `mobile` SET `deleted_at` = '".$this->deleted_at."' WHERE `mobile`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message('<div class="alert alert-danger">
                                    <strong>Info!</strong> Data has been trashed successfully
                                </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }



    }

    public function trashed(){
        $allResult=array();
        $query= "SELECT * FROM `mobile` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $allResult[]=$row;
        }
        return $allResult;
    }
    public function recover(){
        $query="UPDATE `mobile` SET `deleted_at` = NULL WHERE `mobile`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message('<div class="alert alert-danger">
                                    <strong>Info!</strong> Data has been recovered successfully
                                </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }



    }
//    public function recoverMultiple($ids=Array()){
//        if(is_array($ids) && count($ids)>0){
//            $_ids=implode(',',$ids);
//            $query= "UPDATE `mobile` SET `deleted_at` = NULL WHERE `mobile`.`id` IN($_ids)";
//
//            $result = mysqli_query($this->conn,$query);
//
//            if($result){
//                Message::message("Book title is recovered successfully.");
//            }else{
//                Message::message(" Cannot recover.");
//            }
//
//            Utility::redirect('index.php');
//        }else{
//            Message::message('No id avaiable. Sorry !');
//            return Utility::redirect('index.php');
//        }
//
//
//
//    }
//    public function deleteMultiple($ids=Array()){
//        if(is_array($ids) && count($ids)>0){
//            $_ids=implode(',',$ids);
//            $query= "DELETE FROM `mobile` WHERE `mobile`.`id` IN($_ids)";
//
//            $result = mysqli_query($this->conn,$query);
//
//            if($result){
//                Message::message("Book title is deleted successfully.");
//            }else{
//                Message::message(" Cannot delete.");
//            }
//
//            Utility::redirect('index.php');
//        }else{
//            Message::message('No id avaiable. Sorry !');
//            return Utility::redirect('index.php');
//        }
//
//
//
//    }


        public function recoverMultiple($IDs=Array()){
            if((is_array($IDs))&& count($IDs)) {
                $_ids= implode(',',$IDs);
                $query = "UPDATE `mobile` SET `deleted_at` = NULL WHERE `mobile`.`id` IN($_ids)";
                $result= mysqli_query($this->conn,$query);


            if($result){
                Message::message("Book title is recovered successfully.");
            }else{
                Message::message(" Cannot recover.");
            }

            Utility::redirect('index.php');
        }else{
            Message::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }



        }
    public function deleteMultiple($IDs=Array()){
        if((is_array($IDs))&& count($IDs)) {
            $_ids= implode(',',$IDs);
            $query = "DELETE FROM `mobile` WHERE `mobile`.`id` IN($_ids)";
            $result= mysqli_query($this->conn,$query);


            if($result){
                Message::message("Book title has been deleted successfully.");
            }else{
                Message::message(" Cannot delete.");
            }

            Utility::redirect('index.php');
        }else{
            Message::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }



    }

    //Pagination
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `hobby`";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }


    public function paginate($pageStartFrom=0,$Limit=5){
        $_paginatedData=array();
        $query="SELECT * FROM `hobby` LIMIT ".$pageStartFrom.",".$Limit;
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_assoc($result)){
            $_paginatedData[]=$row;

        }
        return $_paginatedData;

    }












}
