<?php
session_start();
//var_dump($_POST);
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\ProfilePicture\ImageUploader;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

$profile_picture = new ImageUploader();
//$profile_picture->prepare($_GET)->activeImage();
$imageData=$profile_picture->imageData();
//var_dump($imageData);
//die();

//Pagination
if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else {
    $_SESSION['itemPerPage']=5;
}

$itemPerPage= $_SESSION['itemPerPage'];
$totalItem= $profile_picture->count();
$totalPage= ceil($totalItem/$itemPerPage);

if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}
else {
    $pageNumber=$_GET['pageNumber']=1;
}



$pagination="";
for($i=1;$i<=$totalPage;$i++){
    $active=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$active'><a href='index.php?pageNumber=".$i."'>".$i."</a></li>";

}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allData=$profile_picture->paginate($pageStartFrom,$itemPerPage);

$nextPage= $pageNumber+1;
$prevPage= $pageNumber-1;



?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link href="../../../Resources/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<body>

<div class="container">

    <h2>All Profile list</h2>
    <a href="create.php" class="btn btn-success" role="button">Create again</a>
    <a href="trashed.php" class="btn btn-primary" role="button">Trashed Data</a>
    <div id="message">
        <?php
        if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
            echo Message::message();
        } ?>

    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select number of items you want to see (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5){echo "selected";}?>>5</option>
                <option <?php if($itemPerPage==10){echo "selected";}?>>10</option>
                <option <?php if($itemPerPage==15){echo "selected";}?>>15</option>
                <option <?php if($itemPerPage==20){echo "selected";}?>>20</option>
                <option <?php if($itemPerPage==25){echo "selected";}?>>25</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
    </br>
    <div class="panel panel-primary">
        <div class="panel-heading">Profile Picture</div>
        <div class="panel-body">
            <?php if(!empty($imageData['images'])){?>
                <img src="../../../Resources/Images/<?php echo $imageData['images']?>"
                      class="img-thumbnail"
                      height="200px"
                      width="200px">
            <?php }else echo "No active item" ?>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Action</th>
            </thead>
            <tbody>
            <?php $sl=0;
            foreach ($allData as $data){
                $sl++;
                ?>
                <tr>
                    <td><?php echo $sl+$pageStartFrom?></td>
                    <td><?php echo $data['id']?></td>
                    <td><?php echo $data['name']?></td>
                    <td><img src="../../../Resources/Images/<?php echo $data['images']?> " class="img-thumbnail" alt="image" height="200px" width="200px"></td>
                    <td>
                        <?php if(($data['active'])==0){?>
                        <a href="active.php?id=<?php echo $data['id']?>" class="btn btn-info" role="button">Make Active</a>
                        <?php } ?>
                        <?php if(($data['active'])==1){?>
                            <a href="deactivated.php?id=<?php echo $data['id']?>" class="btn btn-danger" role="button">Make Deactive</a>
                        <?php } ?>
                        <a href="view.php?id=<?php echo $data['id']?>" class="btn btn-info" role="button">View</a>
                        <a href="edit.php?id=<?php echo $data['id']?>" class="btn btn-success" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $data['id']?>" class="btn btn-danger" id="delete" role="button">Delete</a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <ul class="pagination">
        <?php if(array_key_exists('pageNumber',$_GET)){
                if(($_GET['pageNumber'])>1){           
                    echo "<li><a href='index.php?pageNumber=".$prevPage."'>Prev</a></li>";
            }
        }
        ?>
        <?php echo $pagination ?>

        <?php if(array_key_exists('pageNumber',$_GET)){
            if(($_GET['pageNumber'])<$totalPage){
                echo "<li><a href='index.php?pageNumber=".$nextPage."'>Next</a></li>";
            }
        }
        ?>
    </ul>





</div>

<script>
    $("#message").show().delay(5000).fadeOut();
    $("#delete").bind('click',function(e) {
        var deleteItem= confirm("Are you sure you want to delete?");
        if(!deleteItem){
            e.preventDefault();
        }
    });

</script>
</body>
</html>
