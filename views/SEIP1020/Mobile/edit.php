<?php

//var_dump($_POST);
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\Mobile\Mobile;
$mobile = new Mobile();
$data=$mobile->prepare($_GET)->show();



?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <link href="../../../Resources/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet"/>
</head>
<body>

<div class="container">
    <h2>Horizontal form</h2>
    <form class="form-horizontal" role="form" action="update.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" >Update Book Title:</label>
            <div class="col-sm-10">
                <input type="hidden" name="id" value="<?php echo $data['id'] ?>" >
                <input type="text" class="form-control" name="title" value="<?php echo $data['title']?>" id="text" placeholder="Enter book Title">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>
