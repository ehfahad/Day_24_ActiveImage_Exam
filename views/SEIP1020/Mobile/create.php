<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../../Resources/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet"/>
</head>
<body>


<div class="container">

    <h2>Add Moble title</h2>
    <form class="form-horizontal" role="form" action="store.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" >Enter Book Title:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="title" id="text" placeholder="Enter book Title">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</body>
</html>
