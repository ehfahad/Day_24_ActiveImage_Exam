<?php

//var_dump($_POST);
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\Hobby\Hobby;
$hobby = new Hobby();
$data=$hobby->prepare($_GET)->show();
$array_hobby= explode(",",$data['hobby']);
//var_dump($array_hobby);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Hobby</h2>
    <form role="form" action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $data['id']?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" value="<?php echo $data['name']?>" class="form-control">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Cricket"
                          <?php if(in_array('Cricket',$array_hobby)){
                              echo "Checked";
                          } ?>>Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Football"  <?php if(in_array('Football',$array_hobby)){
                    echo "Checked";
                } ?>>Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Gardening"  <?php if(in_array('Gardening',$array_hobby)){
                    echo "Checked";
                } ?>>Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Stamp Collection"  <?php if(in_array('Stamp Collection',$array_hobby)){
                    echo "Checked";
                } ?>>Stamp collection</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Adventure"  <?php if(in_array('Adventure',$array_hobby)){
                    echo "Checked";
                } ?>>Adventure</label>
        </div>

        <input type="submit" name="Update" value="Update">
    </form>
</div>

</body>
</html>







